<!DOCTYPE html>



<html>
    <head>
        <meta charset="utf-8" />
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <link rel="stylesheet" href="../css/contact.css" />
        <link rel="icon" type="image/png" href="/icon/logo.png">
        <title>Portfolio Mathéo Sital</title>
    </head>
    <body>
        <nav class="navbar">
            <a href="index.html">
                <img src="/icon/logo.png">
            </a>
            <ul>
                <li><a href="index.html">Acceuil</a></li>
                <li><a href="Competence.html">Compétence</a></li>
                <li><a href="veille.html">Veille</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
        <div class="head">
            <h1>Contacts</h1>
            <p>Pour en savoir plus, pour obtenir une information...</p>
        </div>        
        <div class="head2">
            <h1>Envoyer un mail</h1>
            <form method="POST">
                <label>Votre nom (obligatoire):</label>
                <input type="text" name="nom" required></br></br>
                
                <label>Votre e-mail (obligatoire):</label>
                <input type="text" name="email" required></br></br>
                
                <label>Message (obligatoire):</label>
                <textarea  name="message" required></textarea></br></br>
                
                <div class="g-recaptcha" data-sitekey="6LdhUksaAAAAANZt3G0M3yXcdX0C0cLcoQRV9d5r" id="captcha"></div>
                
                <?php
                    require_once 'recaptcha/autoload.php';
                    if(isset($_POST['submitpost'])) {
                        $captcha = $_POST['g-recaptcha-response'];
                    $clesecrete = "6LdhUksaAAAAAMOnATr9ooeHgCVMy9iVFcTg9YCC";
                    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$clesecrete&response=$captcha";
                    $reponse = file_get_contents($url);
                    $reponse = json_decode($reponse);
                    if($reponse ->success){
                    }else{
                        $msg = "Vous devez remplir le captcha";
                    }
                }
                ?>

                <button type="submit" value="valider" name="submitpost">Envoyer</button>
                <?php
                    
                ?>
            </form>
            <?php
            if(isset($_POST['message']) AND $reponse ->success AND filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            {
                $res = mail('matheosital12011@gmail.com', 'Envoi depuis la page Contact', $_POST['message'], 'From: '.$_POST['email']);
                if($res){
                    $val = 'Votre message a été envoyé.';
                    echo '<script type="text/javascript">window.alert("'.$val.'");</script>';
                }
                else
                {
                    $val = 'Erreur';
                    echo '<script type="text/javascript">window.alert("'.$val.'");</script>';                  
                }
            }
            ?>
        </div>
        <div class="footer">
            <p>MATHEO SITAL 2020-2021</p>
            <a href="#"><img src="/icon/ld.png"></a>
        </div>
    </body>
</html>